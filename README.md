# CERIcompiler

A simple compiler.
From : Pascal-like imperative LL(k) langage
To : 64 bit 80x86 assembly langage (AT&T)

**Download the repository :**

> git clone git@framagit.org:Wiak/moncompilateur.git

**Build the compiler and test it :**

> make test

**Have a look at the output :**

> gedit test.s

**Debug the executable :**

> ddd ./test

**Commit the new version :**

> git commit -a -m "What's new..."

**Send to your framagit :**

> git push -u origin master

**Get from your framagit :**

> git pull -u origin master

**This version Can handle :**

>// Program := [DeclarationPart] StatementPart <br/>
// DeclarationPart := "[" Letter {"," Letter} "]"<br/>
// StatementPart := Statement {";" Statement} "."<br/>
// Statement := AssignementStatement | WhileStatement<br/>

>// AssignementStatement := Letter "=" Expression<br/>
// WhileStatement := "WHILE" Expression "DO" Statement<br/>
// IfStatement := "IF" Expression "THEN" Statement [ "ELSE" Statement ]<br/>
// ForStatement := "FOR" AssignementStatement ("To"|"DownTo") SimpleExpression "DO" Statement<br/>
// BlockStatement := "BEGIN" Statement { ";" Statement } "END"<br/>
// DisplayStatement := "DISPLAY" Expression<br/>


>// Expression := SimpleExpression [RelationalOperator SimpleExpression]<br/>
// SimpleExpression := Term {AdditiveOperator Term}<br/>
// Term := Factor {MultiplicativeOperator Factor}<br/>
// Factor := Number | Letter | "(" Expression ")"| "!" Factor<br/>
// Number := Digit{Digit}<br/>
// CharConst := Letter<br/>

>// AdditiveOperator := "+" | "-" | "||"<br/>
// MultiplicativeOperator := "*" | "/" | "%" | "&&"<br/>
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  <br/>
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"<br/>
// Letter := "a"|...|"z"<br/>